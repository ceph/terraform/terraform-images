# Terraform images

This is a copy of [Gitlab terraform image](https://gitlab.com/gitlab-org/terraform-images/).
But build with Cern CentOS image so that we are in a CERN environment with our
certificates installed etc.

With this image you can use the [Gitlab Terraform template](https://docs.gitlab.com/ee/user/infrastructure/iac/#use-a-terraform-template)
in the same way that you would with the official images. You just need to
override the image like this:

```
image:
  name: gitlab-registry.cern.ch/ceph/terraform/terraform-images:$VERSION_HERE
```

## Additions

We also add the following tools compared to the Gitlab image:
- terragrunt
