ARG BASE_IMAGE=registry.gitlab.com/gitlab-org/terraform-images/stable:latest
FROM $BASE_IMAGE

FROM gitlab-registry.cern.ch/linuxsupport/cs9-base

COPY krb5.conf /etc/krb5.conf

ARG TERRAGRUNT_VERSION=v0.37.4
RUN dnf install -y jq curl git openssh diffutils && \
    dnf -y install centos-release-ceph-pacific && \
    dnf install -y libcephfs-devel librbd-devel librados-devel && \
    dnf clean all && \
    curl -Lo /usr/local/bin/terragrunt \
        https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 && \
    chmod +x /usr/local/bin/terragrunt

COPY --from=0 /usr/local/bin/terraform /usr/local/bin/terraform
COPY --from=0 /usr/bin/gitlab-terraform /usr/bin/gitlab-terraform
RUN line=$(sed -n '/case/=' /usr/bin/gitlab-terraform) && \
    head -n $(( line - 1)) /usr/bin/gitlab-terraform > /usr/bin/gitlab-terraform-env && \
    chmod +x /usr/bin/gitlab-terraform-env

WORKDIR /

ENTRYPOINT []
